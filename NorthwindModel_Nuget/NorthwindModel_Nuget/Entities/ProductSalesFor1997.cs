// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.5
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning


namespace NorthwindModel_Nuget.Entities
{

    // Product Sales for 1997
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.29.1.0")]
    public class ProductSalesFor1997
    {
        public string CategoryName { get; set; } // CategoryName (Primary key) (length: 15)
        public string ProductName { get; set; } // ProductName (Primary key) (length: 40)
        public decimal? ProductSales { get; set; } // ProductSales
    }

}
// </auto-generated>
