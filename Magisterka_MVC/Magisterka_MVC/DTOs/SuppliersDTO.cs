﻿using NorthwindModel_Nuget.Entities;

namespace Magisterka_MVC.DTOs
{
    public class SuppliersDTO
    {


        public int SupplierId { get; set; } // SupplierID (Primary key)
        public string CompanyName { get; set; } // CompanyName (length: 40)
        public string ContactName { get; set; } // ContactName (length: 30)
        public string ContactTitle { get; set; } // ContactTitle (length: 30)
        public string Address { get; set; } // Address (length: 60)
        public string City { get; set; } // City (length: 15)
        public string Region { get; set; } // Region (length: 15)
        public string PostalCode { get; set; } // PostalCode (length: 10)
        public string Country { get; set; } // Country (length: 15)
        public string Phone { get; set; } // Phone (length: 24)
        public string Fax { get; set; } // Fax (length: 24)
        public string HomePage { get; set; } // HomePage (length: 1073741823)



        public SuppliersDTO(Suppliers supplier)
        {
            this.SupplierId = supplier.SupplierId;
            this.CompanyName = supplier.CompanyName;
            this.ContactName = supplier.ContactName;
            this.ContactTitle = supplier.ContactTitle;
            this.Address = supplier.Address;
            this.City = supplier.City;
            this.Region = supplier.Region;
            this.PostalCode = supplier.PostalCode;
            this.Country = supplier.Country;
            this.Phone = supplier.Phone;
            this.Fax = supplier.Fax;
            this.HomePage = supplier.HomePage;
        }


        public SuppliersDTO()
        {

        }


        public void ApplyToEntity(Suppliers entity) {

            entity.SupplierId = SupplierId;
            entity.CompanyName = CompanyName;
            entity.ContactName = ContactName;
            entity.ContactTitle = ContactTitle;
            entity.Address = Address;
            entity.City = City;
            entity.Region = Region;
            entity.PostalCode = PostalCode;
            entity.Country = Country;
            entity.Phone = Phone;
            entity.Fax = Fax;
            entity.HomePage = HomePage;

        }



    }
}