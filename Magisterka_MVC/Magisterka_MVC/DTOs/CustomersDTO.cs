﻿using System;
using NorthwindModel_Nuget.Entities;

namespace Magisterka_MVC.DTOs
{
    public class CustomersDTO
    {
        public string CustomerId { get; set; } // CustomerID (Primary key) (length: 5)
        public string CompanyName { get; set; } // CompanyName (length: 40)
        public string ContactName { get; set; } // ContactName (length: 30)
        public string ContactTitle { get; set; } // ContactTitle (length: 30)
        public string Address { get; set; } // Address (length: 60)
        public string City { get; set; } // City (length: 15)
        public string Region { get; set; } // Region (length: 15)
        public string PostalCode { get; set; } // PostalCode (length: 10)
        public string Country { get; set; } // Country (length: 15)
        public string Phone { get; set; } // Phone (length: 24)
        public string Fax { get; set; } // Fax (length: 24)


        public CustomersDTO()
        {

        }


        public CustomersDTO(Customers customer)
        {
            this.CustomerId = customer.CustomerId;
            this.CompanyName = customer.CompanyName;
            this.ContactName = customer.ContactName;
            this.ContactTitle = customer.ContactTitle;
            this.Address = customer.Address;
            this.City = customer.City;
            this.Region = customer.Region;
            this.PostalCode = customer.PostalCode;
            this.Country = customer.Country;
            this.Phone = customer.Phone;
            this.Fax = customer.Fax;
        }


        public void ApplyToEntity(Customers entity)
        {


            entity.CustomerId = CustomerId;
            entity.CompanyName = CompanyName;
            entity.ContactName = ContactName;
            entity.ContactTitle = ContactTitle;
            entity.Address = Address;
            entity.City = City;
            entity.Region = Region;
            entity.PostalCode = PostalCode;
            entity.Country = Country;
            entity.Phone = Phone;
            entity.Fax = Fax;        

        }


    }
}