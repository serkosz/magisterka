﻿using System;
using NorthwindModel_Nuget.Entities;

namespace Magisterka_MVC.DTOs
{
    public class OrderDTO
    {

        public int OrderID { get; set; }
        public DateTime? OrderDate { get; set; }

        public string Address { get; set; }



        public OrderDTO()
        {
        }


        public OrderDTO(Orders order)
        {

            this.OrderID = order.OrderId;
            this.OrderDate = order.OrderDate;
            this.Address = order.ShipAddress + order.ShipPostalCode + order.ShipCity + order.ShipCountry;

        }


    }
}