﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NorthwindModel_Nuget.Entities;

namespace Magisterka_MVC.DTOs
{
    public class OrderDetailsDTO
    {

        public int OrderId { get; set; } // OrderID (Primary key)
        public int ProductId { get; set; } // ProductID (Primary key)
        public decimal UnitPrice { get; set; } // UnitPrice
        public short Quantity { get; set; } // Quantity
        public float Discount { get; set; } // Discount


        public OrderDetails AddEntity()
        {

            var entity = new OrderDetails();

            entity.OrderId = this.OrderId;
            entity.Orders = null;
            entity.Products = null;
            entity.ProductId = this.ProductId;
            entity.UnitPrice = this.UnitPrice;
            entity.Quantity = this.Quantity;
            entity.Discount = this.Discount;


            return entity;
        }


    }
}