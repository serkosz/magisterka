﻿using System.Web.Optimization;


namespace Magisterka_MVC.App_Start
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {


            bundles.Add(new ScriptBundle("~/bundles/npm").Include(
                "~/ClientPackage/node_modules/jquery/dist/jquery.js",
                "~/ClientPackage/node_modules/angular/angular.min.js",
                "~/ClientPackage/node_modules/bootstrap/dist/js/bootstrap.js",
                "~/ClientPackage/node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/AngularCtrl").Include(
                "~/Scripts/module.js",
                "~/Scripts/Ctrl/CustomerCtrl.js",
                "~/Scripts/Ctrl/SupplierCtrl.js",
                "~/Scripts/Ctrl/SupplierAddCtrl.js",
                "~/Scripts/Ctrl/SupplierEditCtrl.js",
                "~/Scripts/Services/customerService.js",
                "~/Scripts/Services/supplierService.js"
                ));


            bundles.Add(new StyleBundle("~/bundles/npmStyles").Include(

                "~/ClientPackage/node_modules/bootstrap/dist/css/bootstrap.css",
                "~/ClientPackage/node_modules/bootstrap/dist/css/bootstrap-theme.css"

                ));

        }

    }
}