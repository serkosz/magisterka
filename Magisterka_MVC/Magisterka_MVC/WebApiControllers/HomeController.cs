﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Magisterka_MVC.DTOs;
using GenericRepository.UnitOfWork;
using GenericRepository.Interfaces;

namespace Magisterka_MVC.WebApiControllers
{
    public class HomeController : ApiController
    {
        private readonly IUnitOfWork uow;
        public IOrders orderRepo { get; set; }
        public IOrderDetails orderDetailsRepo { get; set; }

        public HomeController()
        {
            this.uow = new UnitOfWork();
            //inicjalizacja repozytoriów
            this.orderRepo = uow.Orders;
            this.orderDetailsRepo = uow.OrderDetails;

        }



        [HttpGet]
        public IHttpActionResult GetOrders()
        {
            var result = uow.Orders.GetAll().AsEnumerable();

            var dto = result.Select(x => new OrderDTO(x)).ToList();

            return Ok(dto);
        }


        [HttpGet]
        public IHttpActionResult GetOrders10()
        {
            var result = uow.Orders.GetAll().Take(10).AsEnumerable();

            var dto = result.Select(x => new OrderDTO(x)).ToList();

            return Ok(dto);

        }

        [HttpGet]
        [Route("api/home/orderbyid/{id}")]

        public IHttpActionResult RepoOrderById(int id)
        {

            var order = orderRepo.FindById(id);
            var result = new OrderDTO(order);
            return Ok(result);

        }

        [HttpGet]
        [Route("api/home/deletebyid/{id}")]
        public IHttpActionResult DeleteById(int id)
        {
            var order = orderRepo.FindById(id);

            orderRepo.Delete(order);
            orderRepo.Save();

            return Ok();
        }



        [HttpPost]
        [Route("api/home/upsertOrderDetails")]

        public IHttpActionResult upsertOrderDetails(List<OrderDetailsDTO> details) {

            //id zamowienia
            var orderId = details.Select(x => x.OrderId).FirstOrDefault();

            //wszystkie szczegoly danego zamowienia
            var allDetails = orderDetailsRepo.FindBy(x => x.OrderId == orderId).ToList();

            //jesi szczegoly nie istnieja w details to je usuwamy
            var toDeleteDetails = allDetails.Where(d =>
                                            !details.Where(d1 => d1.OrderId == d.OrderId && d1.ProductId == d.ProductId).Any()).ToList();

            foreach (var delete in toDeleteDetails)
            {
                orderDetailsRepo.Delete(delete);
            }

            //jesli szczegoly istnieja to robimy dla nich update
            var toUpdateDetails = allDetails.Where(d =>
                                            details.Where(d1 => d1.OrderId == d.OrderId && d1.ProductId == d.ProductId).Any()).ToList();

            foreach (var update in toUpdateDetails)
            {
                orderDetailsRepo.Edit(update);
            }

            //nowe rekordy do dodania
            var toInsertDetails = details.Where(d => !allDetails.Where(al => al.OrderId == d.OrderId && al.ProductId == d.ProductId).Any()).ToList();


            foreach (var insert in toInsertDetails)
            {

                var add = insert.AddEntity();

                orderDetailsRepo.Add(add);
            }

            //zapisujemy wszystkie zmiany na koncu
            orderDetailsRepo.Save();

            return Ok();


        }



    }
}
