﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Magisterka_MVC.DTOs;
using GenericRepository.UnitOfWork;
using GenericRepository.Interfaces;
using NorthwindModel_Nuget.Entities;

namespace Magisterka_MVC.WebApiControllers
{
    public class SuppliersController : ApiController
    {

        private readonly IUnitOfWork uow;
        public ISuppliers suppliersRepo { get; set; }


        public SuppliersController()
        {
            uow = new UnitOfWork();
            suppliersRepo = uow.Suppliers;
        }


        [HttpGet]
        public IHttpActionResult GetSuppliers()
        {

            var result = suppliersRepo.GetAll().AsEnumerable();

            var dto = result.Select(x => new SuppliersDTO(x)).ToList();

            return Ok(dto);
        }


        [HttpGet]
        [Route("api/Suppliers/delete/{id}")]
        public IHttpActionResult DeleteSupplier(int id) {

            var supplier = suppliersRepo.FindSupplierById(id);

            suppliersRepo.Delete(supplier);
            suppliersRepo.Save();

            return Ok();          
        }


        [HttpPost]
        [Route("api/Suppliers/add")]
        public IHttpActionResult AddSupplier(SuppliersDTO supplier)
        {

            var entity = new Suppliers();

            supplier.ApplyToEntity(entity);

            suppliersRepo.Add(entity);

            suppliersRepo.Save();

            return Ok();
        }


        [HttpPost]
        [Route("api/Suppliers/update")]
        public IHttpActionResult UpdateSupplier(SuppliersDTO supplier)
        {

            var id = supplier.SupplierId;

            var supp = suppliersRepo.FindSupplierById(id);

            supplier.ApplyToEntity(supp);

            suppliersRepo.Save();

            return Ok();
        }

        [HttpGet]
        [Route("api/Suppliers/get/{id}")]
        public IHttpActionResult GetSupplierById(int id)
        {

            var supplier = suppliersRepo.FindSupplierById(id);
            var result = new SuppliersDTO(supplier);

            return Ok(result);

        }



    }
}
