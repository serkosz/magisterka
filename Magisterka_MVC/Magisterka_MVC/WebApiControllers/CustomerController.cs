﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Magisterka_MVC.DTOs;
using GenericRepository.UnitOfWork;
using GenericRepository.Interfaces;

namespace Magisterka_MVC.WebApiControllers
{
    public class CustomerController : ApiController
    {

        private readonly IUnitOfWork uow;

        public ICustomers customersRepo { get; set; }


        public CustomerController()
        {
            uow = new UnitOfWork();
            customersRepo = uow.Customers;
        }


        [HttpGet]
        public IHttpActionResult GetCustomers()
        {
            var result = uow.Customers.GetAll().AsEnumerable();

            var dto = result.Select(x => new CustomersDTO(x)).ToList();

            return Ok(dto);
        }

        [HttpGet]
        [Route("api/customer/get/{id}")]
        public IHttpActionResult GetCustomerById(string id)
        {
            var customer = customersRepo.FindCustomerById(id);
            var result = new CustomersDTO(customer);

            return Ok(result);
        }


        [HttpPost]
        [Route("api/customer/update")]

        public IHttpActionResult UpdateCustomer(CustomersDTO customer)
        {

            var id = customer.CustomerId;

            var cust = customersRepo.FindCustomerById(id);

            customer.ApplyToEntity(cust);

            customersRepo.Save();

            return Ok();

        }


    }
}
