﻿angular.module('magisterkaModule')
    .controller('SupplierCtrl', ["$scope", 'supplier', function ($scope, supplier) {

        console.log('kod wywolawny w supplierCtrl');


        //pobierz wszystkich dostawców
        $scope.suppliers = null;
        $scope.pobierzWszystkich = function () {

            supplier.getSuppliers().then(function (response) {

                $scope.suppliers = response.data;
                console.log('sppliers', $scope.suppliers);

            });
        };
        $scope.pobierzWszystkich();


        $scope.dodaj = function () {

            window.open('/supplier/add', '_self');

        };

        $scope.edit = function (id) {

            var url = '/supplier/edit/' + id;

            window.open(url, '_self');
        };

        $scope.delete = function (obj) {

            console.log('obj delete', obj);

        };


    }]);