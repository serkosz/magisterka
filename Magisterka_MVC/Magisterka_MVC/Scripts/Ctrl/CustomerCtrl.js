﻿angular.module('magisterkaModule')
    .controller('CustomerCtrl', ["$scope", "$location",'customer', function ($scope, $location, customer) {


    $scope.start = function () {

        $scope.mySessionStorage = sessionStorage.urlParams;

        $scope.$watch('mySessionStorage', function (newVal) {

            var obiekt = JSON.parse(newVal);
            var customerId = obiekt.customerId;

            console.log('customerId', customerId);

            sessionStorage.removeItem('urlParams');


            customer.customerById(customerId).then(function (response) {


                $scope.customer = response.data;

                console.log('customer', $scope.customer);

            })


        });

    };

    $scope.start();


    $scope.save = function () {

        customer.update($scope.customer).then(function () {

            console.log('jest ok');

            $("#komunikat").append('<b id="komunikatSukces" style="color: green;">Sukces! Zapisano wynik.</b>');

        }, function () {

            $("#komunikat").append('<b id="komunikatFail" style="color: red;">Podczas zapisywania wystąpił błąd.</b>');
        });
    }


}]);