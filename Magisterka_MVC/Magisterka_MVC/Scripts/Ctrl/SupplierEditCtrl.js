﻿angular.module('magisterkaModule')
    .controller('SupplierEditCtrl', ["$scope", 'supplier', function ($scope, supplier) {



        $scope.start = function () {

            $scope.mySessionStorage = sessionStorage.urlParams;

            $scope.$watch('mySessionStorage', function (newVal) {

                var obiekt = JSON.parse(newVal);
                var supplierId = obiekt.supplierId;

                console.log('supplierId', supplierId);

                sessionStorage.removeItem('urlParams');


                supplier.getSupplierById(supplierId).then(function (response) {

                    $scope.supplier = response.data;
                    console.log('supplier', $scope.supplier);
                })


            });

        };

        $scope.start();

        $scope.save = function () {


            console.log('obj to save', $scope.supplier);

            supplier.editSupplier($scope.supplier).then(function (response) {

                alert('Klient został zedytowany');                

            });


        };


    }]);