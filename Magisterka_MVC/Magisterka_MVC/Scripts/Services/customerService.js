﻿angular.module('magisterkaModule')
    .factory("customer", ["$http", function ($http) {

        return {

            customerById: function (customerId) {
                return $http({ method: 'GET', url: "/api/customer/get/" + customerId });
            },
            update: function (model) {
                return $http({ url: "/api/customer/update", method: "POST", dataType: 'json', data: model })
            }

        }

    }]);