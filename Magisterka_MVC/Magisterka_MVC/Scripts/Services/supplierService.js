﻿angular.module('magisterkaModule')
    .factory("supplier", ["$http", function ($http) {

        return {

            getSuppliers: function () {
                return $http({ url: "/api/Suppliers/GetSuppliers", method: "GET" });
            },
            getSupplierById: function (supplierId) {
                return $http({ url: "/api/Suppliers/get/" + supplierId, method: "GET" });
            },
            editSupplier: function (supplier) {
                return $http({ url: "/api/Suppliers/update", method: "POST", dataType: 'json', data: supplier })
            },
            addSupplier: function (supplier) {
                return $http({ url: "/api/Suppliers/add", method: "POST", dataType: 'json', data: supplier })
            }
        }
    }]);