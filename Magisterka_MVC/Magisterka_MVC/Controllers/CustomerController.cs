﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magisterka_MVC.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer/Edit/{id}


        [Route("customer/edit/{customerId}")]
        public ActionResult EditCustomer(string customerId)
        {

            if (customerId == null)
            {
                RedirectToAction("Index", "Home");
            }

            Dictionary<string, string> dict = new Dictionary<string, string>();

            if (customerId != null)
            {

                dict.Add("customerId", customerId);
            }

            return View("Customer", null, dict);

        }
    }
}