﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magisterka_MVC.Controllers
{
    public class SupplierController : Controller
    {
        // GET: Supplier
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Add() {
            return View();
        }

        [Route("supplier/edit/{supplierId}")]
        public ActionResult Edit(int? supplierId) {

            if (supplierId == null)
            {
                RedirectToAction("Index", "Home");
            }

            Dictionary<string, int?> dict = new Dictionary<string, int?>();

            if (supplierId != null)
            {

                dict.Add("supplierId", supplierId);
            }


            return View("Edit",null,dict);
        }

    }
}