﻿using NorthwindModel_Nuget.Entities;
using System.Linq;
using GenericRepository.Interfaces;
using System.Data.Entity;

namespace GenericRepository.Repositories
{
    public class OrdersRepo : GenericRepo<Orders>, IOrders
    {

        public OrdersRepo(DbContext context) : base(context)
        {
        }

        public Orders FindById(int orderId)
        {

            var query = this.GetAll().FirstOrDefault(x => x.OrderId == orderId);
            return query;

        }


    }
}
