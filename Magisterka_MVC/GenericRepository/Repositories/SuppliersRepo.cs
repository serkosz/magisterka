﻿using NorthwindModel_Nuget.Entities;
using System.Linq;
using GenericRepository.Interfaces;
using System.Data.Entity;

namespace GenericRepository.Repositories
{
   public class SuppliersRepo : GenericRepo<Suppliers>, ISuppliers
    {

        public SuppliersRepo()
        {
        }

        public SuppliersRepo(DbContext context) : base(context)
        {

        }


        public Suppliers FindSupplierById(int supplierId)
        {

            var query = this.GetAll().FirstOrDefault(x => x.SupplierId == supplierId);
            return query;
        }


    }
}
