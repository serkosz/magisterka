﻿using NorthwindModel_Nuget.Context;
using NorthwindModel_Nuget.Entities;
using System.Linq;
using System.Data.Entity;
using GenericRepository.Interfaces;

namespace GenericRepository.Repositories
{
    public class OrderDetailsRepo : GenericRepo<OrderDetails>, IOrderDetails
    {
        public OrderDetailsRepo(DbContext context) : base(context)
        {

        }

    }
}
