﻿using NorthwindModel_Nuget.Entities;
using System.Linq;
using GenericRepository.Interfaces;
using System.Data.Entity;

namespace GenericRepository.Repositories
{
   public class CustomersRepo : GenericRepo<Customers>, ICustomers
    {

        public CustomersRepo(DbContext context) : base(context)
        {

        }


        public Customers FindCustomerById(string id)
        {

            var query = this.GetAll().FirstOrDefault(x => x.CustomerId == id);
            return query;
        }


    }
}
