﻿using NorthwindModel_Nuget.Entities;

namespace GenericRepository.Interfaces
{
    public interface ICustomers : IGenericRepository<Customers>
    {
         Customers FindCustomerById(string id);
    }

}
