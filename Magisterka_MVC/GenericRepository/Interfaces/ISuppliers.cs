﻿using NorthwindModel_Nuget.Entities;

namespace GenericRepository.Interfaces
{
   public interface ISuppliers : IGenericRepository<Suppliers>
    {


        Suppliers FindSupplierById(int supplierId);

    }
}
