﻿using NorthwindModel_Nuget.Entities;

namespace GenericRepository.Interfaces
{
   public interface IOrderDetails : IGenericRepository<OrderDetails>
    {
    }
}
