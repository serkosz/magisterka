﻿using NorthwindModel_Nuget.Entities;

namespace GenericRepository.Interfaces
{
   public interface IOrders : IGenericRepository<Orders>
    {


        Orders FindById(int orderId);

    }
}
