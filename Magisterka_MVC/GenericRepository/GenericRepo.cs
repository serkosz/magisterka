﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace GenericRepository
{
    public abstract class GenericRepo<T> :
        IGenericRepository<T> where T : class, new()
    {

        //when pass C which is DbContext
        //private C _entities = new C();
        //public C Context
        //{
        //    get { return _entities; }
        //    set { _entities = value; }
        //}
        protected DbContext _entities;
        protected readonly IDbSet<T> _dbset;


        public GenericRepo()
        {
        }

        public GenericRepo(DbContext context)
        {
            _entities = context;
            _dbset = context.Set<T>();
        }


        public virtual IQueryable<T> GetAll()
        {

            IQueryable<T> query = _entities.Set<T>();
            return query;
        }


        public IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {

            IQueryable<T> query = _dbset.Where(predicate);
            return query;
        }

        public virtual void Add(T entity)
        {
            _dbset.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            _dbset.Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            _entities.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }
    }
}
