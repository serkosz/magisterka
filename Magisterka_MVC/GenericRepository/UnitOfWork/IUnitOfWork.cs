﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using NorthwindModel_Nuget.Entities;
using System.Data.Entity;
using GenericRepository.Interfaces;

namespace GenericRepository.UnitOfWork
{
   public interface IUnitOfWork : IDisposable
    {
        void Save();
        //DbRawSqlQuery<T> Query<T>(string query) where T : class;
        //DbRawSqlQuery<T> Query<T>(string query, params object[] paramsObject) where T : class;
        
        IOrders Orders { get; }
        IOrderDetails OrderDetails { get; }

        ICustomers Customers { get; }
        ISuppliers Suppliers { get; }

    }
}
