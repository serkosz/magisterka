﻿using NorthwindModel_Nuget.Context;
using GenericRepository.Interfaces;
using GenericRepository.Repositories;
using System.Collections;
using System;

namespace GenericRepository.UnitOfWork
{
   public sealed class UnitOfWork : IUnitOfWork
    {

        private bool _disposed;


        public NorthwindDbContext Context { get; private set; }


        public IOrders Orders { get; set; }
        public IOrderDetails OrderDetails { get; set; }
        public ICustomers Customers { get; set; }
        public ISuppliers Suppliers { get; set; }

        public UnitOfWork()
        {
            Context = new NorthwindDbContext();
            Orders = new OrdersRepo(Context);  
            OrderDetails = new OrderDetailsRepo(Context);
            Customers = new CustomersRepo(Context);
            Suppliers = new SuppliersRepo(Context);
        }

        public UnitOfWork(NorthwindDbContext context) : this()
        {
            Context = context;
        }


        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    this.Context.Dispose();
                }
            }

            this._disposed = true;
        }


        public void Save()
        {
            this.Context.SaveChanges();
        }

    }
}
