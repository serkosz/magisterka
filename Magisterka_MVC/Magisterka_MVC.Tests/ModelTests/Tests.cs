﻿using System.Linq;
using NUnit.Framework;
using NorthwindModel_Nuget.Context;

namespace Magisterka_MVC.Tests.ModelTests
{

    [TestFixture]
    class Tests
    {

        [Test]
        public void ShouldDeleteOrder() {


            using (var db = new NorthwindDbContext()) {

                var deleteOrderId = db.Orders.FirstOrDefault();


                db.Orders.Remove(deleteOrderId);
                db.SaveChanges();
            }

        }


    }
}
